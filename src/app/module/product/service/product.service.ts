import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../model/product';
import { environment } from 'src/environments/environment';
import { ProductViewComponent } from '../product-view/product-view.component';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private SERVICE_URL = "/products";

  constructor(private httpClient: HttpClient) {}

  public findAll() : Observable<Product[]> {
    return this.httpClient.get<Product[]>(`${environment.BASE_URL + this.SERVICE_URL}`);
  }

  public findById(id?: number) : Observable<Product> {
    return this.httpClient.get<Product>(`${environment.BASE_URL + this.SERVICE_URL}/${id}`);
  }

  public save(product: Product) : Observable<any> {
    return this.httpClient.post(`${environment.BASE_URL + this.SERVICE_URL}`, product);
  }

  public addToOrder(id: string, quantity : number = 1) : Observable<any> {
    return this.httpClient.post(`${environment.BASE_URL + this.SERVICE_URL}/addToOrder`, {
      productId : id,
      quantity : quantity
    });
  }

  public updateOrder(id: string, quantity : number = 1) : Observable<any> {
    return this.httpClient.post(`${environment.BASE_URL + this.SERVICE_URL}/updateOrder`, {
      productId : id,
      quantity : quantity
    });
  }

  public removeFromOrder(id: string) : Observable<any> {
    return this.httpClient.post(`${environment.BASE_URL + this.SERVICE_URL}/removeFromOrder`, {
      productId : id
    });
  }

  public delete(id: string) : Observable<any> {
    return this.httpClient.delete(`${environment.BASE_URL + this.SERVICE_URL}/${id}`);
  }
}
