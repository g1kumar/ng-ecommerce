import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductViewComponent } from './product-view/product-view.component';


const routes: Routes = [
  { path : '', children : [
    { path : '', redirectTo : 'list'},
    { path : 'list', component: ProductListComponent },
    { path : 'add', component: ProductFormComponent },
    { path : ':id', component: ProductFormComponent },
    { path : ':id/edit', component: ProductFormComponent },
  ]}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
