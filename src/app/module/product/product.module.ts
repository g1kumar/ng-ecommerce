import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductRoutingModule } from './product-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { AlertModule } from 'ngx-bootstrap/alert';



@NgModule({
  declarations: [ProductListComponent, ProductFormComponent, ProductViewComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    ReactiveFormsModule, AlertModule,
    SharedModule
  ],
  bootstrap : [ ]
})
export class ProductModule { }
