export class Product {
  _id: string;
  title: string;
  price: number;
  quantity: number;
}
