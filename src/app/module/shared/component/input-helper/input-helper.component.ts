import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-helper',
  templateUrl: './input-helper.component.html',
  styleUrls: ['./input-helper.component.sass'],
  host: { class : ''}
})
export class InputHelperComponent implements OnInit {

  // @Input("fg") formGroup : FormGroup
  @Input("fc") formControl : FormControl
  @Input("fg") formGroup : FormGroup

  constructor() { }

  ngOnInit(): void {
  }

}
