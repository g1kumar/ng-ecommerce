import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-modal',
  templateUrl: './loading-modal.component.html',
  styleUrls: ['./loading-modal.component.sass']
})
export class LoadingModalComponent implements OnInit {

  message : string;

  constructor() { }

  ngOnInit(): void {
  }

}
