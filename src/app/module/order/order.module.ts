import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { SharedModule } from '../shared/shared.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { OrderViewComponent } from './order-view/order-view.component';


@NgModule({
  declarations: [OrderListComponent, OrderFormComponent, OrderViewComponent],
  imports: [
    CommonModule,
    OrderRoutingModule,
    SharedModule,
    // ModalModule.forRoot()
  ]
})
export class OrderModule { }
