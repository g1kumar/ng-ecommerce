import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderFormComponent } from './order-form/order-form.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderViewComponent } from './order-view/order-view.component';


const routes: Routes = [
  { path : '', children : [
    { path : '', redirectTo : 'list', pathMatch: 'full'},
    { path : 'list', component : OrderListComponent },
    { path : ':id', component : OrderFormComponent },
    { path : ':id/view', component : OrderViewComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
