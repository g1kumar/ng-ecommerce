import { EcResponse } from "./ec-response";

export class EcRegisterResponse extends EcResponse {
  message: string
}
