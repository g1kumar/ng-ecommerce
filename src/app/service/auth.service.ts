import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EcLoginResponse } from '../model/ec-login-response';
import { EcRegisterResponse } from '../model/ec-register-response';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _token: string;
  private _email: string;

  constructor(private httpClient: HttpClient) { }

  public login(email: string, password: string): Observable<EcLoginResponse> {
    return this.httpClient.post<EcLoginResponse>(`${environment.BASE_URL}/login`, {
      email: email,
      password: password
    });
  }

  get token(): string {
    this._token = localStorage.getItem("token");
    return this._token;
  }

  set token(token: string){
    this._token = token;
    localStorage.setItem("token", this._token);
  }

  get email(): string {
    this._email = localStorage.getItem("email");
    return this._email;
  }

  set email(email: string){
    this._email = email;
    localStorage.setItem("email", this._email);
  }


  public register(email: string, password: string): Observable<EcRegisterResponse> {
    return this.httpClient.post<EcRegisterResponse>(`${environment.BASE_URL}/register`, {
      email: email,
      password: password
    });
  }

  public logout(){
    this._token = null;
    localStorage.removeItem("token");
    this._email = null;
    localStorage.removeItem("email");
  }
}
