import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModalService } from 'src/app/module/shared/service/common-modal.service';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  alert = {
    type: null,
    isOpen: false,
    message: null
  }

  constructor(private authService: AuthService,
    private modalService: CommonModalService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {

  }

  form = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  })

  ngOnInit(): void {
  }

  login() {
    this.alert.isOpen = false;
    this.form.markAsDirty();
    if (this.form.valid) {
      let ref = this.modalService.showLoading("Signing In");
      this.authService.login(this.form.controls.email.value, this.form.controls.password.value).subscribe(d => {
        console.log(d);
        ref.hide();
        if (d != null && d.error != undefined) {
          this.showErrorAlert(d.error);
        } else {
          this.authService.token = d.token;
          this.authService.email = d.email;
          if (this.activatedRoute.snapshot.queryParams.url){
            this.router.navigateByUrl(this.activatedRoute.snapshot.queryParams.url);
          }else{
            this.router.navigate(['products', 'list']);
          }
        }
      }, err => {
        ref.hide();
        console.log(err);
        this.showErrorAlert(err.error);
      })
    }
  }

  private showErrorAlert(message: string) {
    this.showAlert("danger", true, message);
  }

  private showAlert(type: string, isOpen: boolean, message: string) {
    this.alert.type = type
    this.alert.message = message
    this.alert.isOpen = isOpen;
  }

}
